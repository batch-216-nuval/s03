package com.zuitt.wdc044_s01.models;

import org.springframework.objenesis.instantiator.basic.ObjectInputStreamInstantiator;

import javax.persistence.*;


//  Mark this Java object as a representation of a database via @Entity
@Entity

//  Designate tavle name
@Table(name = "posts")


public class Post {
//  Indicate that this property represents the primary key
    @Id
//  Values for this property will be auto-incremented
    @GeneratedValue
    private Long id;
//  Class properties that represents table columns in a relational databse are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;


//  Empty Constructor
    public Post(){}

//  Parameterized Constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

//  Getters and Setters
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }







}
