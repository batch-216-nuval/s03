package com.zuitt.wdc044_s01.models;


import javax.persistence.*;

@Entity

//  Designate table name
@Table(name = "users")


public class User {

    //  Indicate that this property represents the primary key
    @Id
//  Values for this property will be auto-incremented
    @GeneratedValue
    private Long id;
    //  Class properties that represents table columns in a relational databse are annotated as @Column
    @Column
    private String username;

    @Column
    private String password;


    //  Empty Constructor
    public User(){}

    //  Parameterized Constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //  Getters and Setters

    public Long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }




}
